#!/usr/bin/env python3
import click
import pickle

from hockey.data import import_csv
from hockey.model import GreatsModel

@click.command()
@click.option('--data', type=str, help='imported csv data')
@click.option('--mandatory', type=str, default= None, help='mandatory features to use')
@click.option('--scoring', type=str, help='target score (accuracy, precision, recall')
@click.option('--benchmark', type=str, default=None, help='target benchmark')
@click.option('--model', type=str, help='target model file')
def fmain(data, mandatory, scoring, benchmark, model):

    click.secho('Hockey Greats/Busts classification. Model training.\n\n')

    click.secho(f'--> Training model...')
    try:
        mlst = mandatory.split(',') if mandatory is not None else []
        mdl = GreatsModel(scoring=scoring, benchmark=benchmark, mandatory=mlst)
        mdl.train(data)
        with open(model, 'wb') as f:
            pickle.dump(mdl, f)
    except Exception as e:
        click.secho(f'--> Failure {e}', fg='red')


if __name__ == '__main__':
    fmain()
