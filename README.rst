Data Import
===========


python hockey_import.py --src [SRC] --dst [DATA] --raw

* --src [SRC] - path to a CSV file. Multiple --src options can be specified.
* --dst [DST] - path to a file with imported data (will be created)
* --raw - use raw score. If not specified, percentile will be used

Model Generation
================


python hockey_train.py --data [DATA] --scoring [SCORING] --benchmark [BENCHMARK] --model [MODEL]

* --data [DATA] - path to a file with imported data
* --scoring [SCORING] - scoring to use. May be: accuracy, balanced_accuracy, precision, recall
* --benchmark [BENCHMARK] - may be one of: all, defense, winger, center
* --model [MODEL] - target file to save model to.


Predicting
==========

python hockey_predict.py --model [MODEL] --src [SOURCE] --dst [TARGET]

* --model [MODEL] - model file
* --src [SRC] - source CSV file with features
* --dst [DST] - target CSV with predictions

Package
=======

Sample models and predictions on total data along with source CSV include in this repo.


