import numpy as np
import pandas as pd
import click
import warnings
import matplotlib.pyplot as plt
from sklearn.calibration import CalibratedClassifierCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB, ComplementNB, MultinomialNB, BernoulliNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import precision_recall_fscore_support, balanced_accuracy_score, recall_score, precision_score
from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import accuracy_score
import json
import seaborn as sns
from sklearn.feature_selection import RFECV
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score, cross_validate
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import confusion_matrix
import pickle

warnings.simplefilter("ignore")

MIN_SCORE_GAIN = 0.00001

class GreatsModel:

    _estimator = None
    _selected_features = None
    _scoring = None
    _benchmark = None
    _mandatory = None

    def __init__(self, scoring, benchmark, mandatory=[]):
        self._scoring = scoring
        self._benchmark = benchmark
        self._mandatory = mandatory

    def read_data(self, csv_fname: str, yread=True):
        df = pd.read_csv(csv_fname)
        df = df.set_index('Athlete Name')
        if self._benchmark is not None:
            df = df[(df.Benchmark == self._benchmark) | (df.Benchmark == 'all')]
        X = df.drop(labels=['Team', 'Benchmark'], axis='columns')
        y = df['Team'].astype('category').cat.codes if yread else None

        return X, y

    def get_score(self, x, y):
        clf = GaussianNB()
        return np.mean(cross_val_score(clf, x, y, scoring=self._scoring, cv=LeaveOneOut()))

    def train(self, csv_fname: str):

        click.secho('--> Reading data...')

        X, y = self.read_data(csv_fname)

        allfeatures = set(X.columns)

        for f in self._mandatory:
            if f not in allfeatures:
                raise RuntimeError(f'{f} is not a valid feature.')

        used_features = self._mandatory

        current_best_score = None

        click.secho('--> Selecting features...')

        while len(allfeatures) > 0:
            add_scores = {}
            for newfeature in allfeatures:
                local_features = used_features + [newfeature]
                lX = X[local_features]
                add_scores[newfeature] = self.get_score(lX, y)

            best_feature = max(add_scores.keys(), key=(lambda key: add_scores[key]))
            best_score = add_scores[best_feature]

            gain = best_score - current_best_score if current_best_score is not None else best_score

            if current_best_score is None or gain > MIN_SCORE_GAIN:
                current_best_score = best_score
                used_features = used_features + [best_feature]
                allfeatures.remove(best_feature)
                click.secho(f'--> Selected {best_feature}. Best score: {current_best_score}')

            if gain <= MIN_SCORE_GAIN:
                break

        self._selected_features = used_features
        self._estimator = GaussianNB().fit(X[self._selected_features], y)
        y_pred = self._estimator.predict(X[self._selected_features])
        click.secho(f'--> Final train scores: balanced_accuracy: {balanced_accuracy_score(y, y_pred)},'
                    f' Precision: {precision_score(y, y_pred)}, Recall: {recall_score(y, y_pred)}')

    def predict(self, csv_fname: str, target_csv_fname: str):
        X, _ = self.read_data(csv_fname, yread=False)
        X = X[self._selected_features]
        y_proba = self._estimator.predict_proba(X)
        X['Bust'] = y_proba[:, 0]
        X['Greats'] = y_proba[:, 1]
        X[['Bust', 'Greats']].to_csv(target_csv_fname)



