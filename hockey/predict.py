import numpy as np
import pickle
import json
from sklearn.naive_bayes import GaussianNB

SELECTED_FEATURES = ['facet_imagination', 'facet_liberalism', 'big5_conscientiousness',
                     'facet_cautiousness', 'facet_orderliness', 'big5_extraversion',
                     'facet_gregariousness', 'facet_altruism', 'facet_cooperation',
                     'facet_modesty', 'facet_morality', 'facet_trust', 'big5_neuroticism',
                     'facet_anger', 'facet_anxiety', 'facet_immoderation',
                     'facet_vulnerability', 'need_closeness', 'need_excitement', 'need_love',
                     'value_conservation', 'value_self_enhancement']



class GreatinessPredictionModel:

    _clf = None  # GaussianNB

    def __init__(self, fname):
        with open(fname, 'rb') as f:
            self._clf = pickle.load(f)


    def predict(self, data: dict):
        return self._clf.predict(data)

