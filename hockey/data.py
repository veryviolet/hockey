import pandas as pd
from glob import glob
import click
pd.options.mode.chained_assignment = None

BENCHMARKS = {
    'all': ['NHL'],
    'goaltener': ['NHL Goaltender'],
    'defense': ['NHL D', 'NHL Defense'],
    'winger': ['Winger', 'NHL Winger'],
    'center': ['NHL C', 'NHL Center', 'NBA Center']
}

def import_csv(source: list, target: str, raw: bool):
    src_dfs = []

    for src in glob(source):
        click.secho(f'--> Reading {src}.. ', nl=False)
        try:
            xls = pd.ExcelFile(src)
            for sht in xls.sheet_names:
                click.secho(f'    ... Importing sheet {sht}.. ', nl=False)
                sdf = pd.read_excel(xls, sht)
                src_dfs.append(sdf)
                click.secho('.')
        except Exception as e:
            click.secho(f'Failed: {e}.')

    df = pd.concat(src_dfs)
    df = df.dropna(how='all')
    df.Benchmark[df.Benchmark.isna()] = 'all'
    for key, toreplace in BENCHMARKS.items():
        for onereplace in toreplace:
            df.Benchmark[df.Benchmark == onereplace] = key
    df = df[df['Athlete Name'] != 'END']
    df['Team'] = df['Team'].apply(lambda x: 'Greats' if 'Great' in x else 'Bust')


    rdf = df[['Athlete Name', 'Team', 'Benchmark']].set_index('Athlete Name')
    rdf = rdf[~rdf.index.duplicated(keep='first')]

    use_score = 'raw_score' if raw else 'percentile'

    for trait in df.trait_id.unique():
        train_values = \
            df[df.trait_id == trait][['Athlete Name', use_score]].set_index('Athlete Name')[use_score]
        train_values = train_values[~train_values.index.duplicated(keep='first')]

        rdf[trait] = train_values

    rdf.to_csv(target)

