#!/usr/bin/env python3
import click

from hockey.data import import_csv


@click.command()
@click.option('--src', type=str, help='source files wildcard')
@click.option('--dst', type=str, help='result csv')
@click.option('--raw/--no-raw', default=False, help='use raw score')
def fmain(src, dst, raw):

    click.secho('Hockey Greats/Busts classification.\n\n')

    click.secho(f'--> Importing...')
    try:
        import_csv(src, dst, raw)
    except Exception as e:
        click.secho(f'--> Failure {e}', fg='red')


if __name__ == '__main__':
    fmain()
