#!/usr/bin/env python3
import click

from hockey.data import import_csv
from hockey.model import GreatsModel
import pickle



@click.command()
@click.option('--model', type=str, help='model file')
@click.option('--src', type=str, help='source csv')
@click.option('--dst', type=str, help='target csv')
def fmain(model, src, dst):

    click.secho('Hockey Greats/Busts classification. Predict\n\n')

    click.secho(f'--> Loading model...')
    try:
        with open(model, 'rb') as f:
            mdl = pickle.load(f)
    except Exception as e:
        click.secho(f'--> Failure {e}', fg='red')

    click.secho(f'--> Predicting...')
    try:
        mdl.predict(src, dst)
    except Exception as e:
        click.secho(f'--> Failure {e}', fg='red')


if __name__ == '__main__':
    fmain()
